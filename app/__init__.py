from flask import Flask
from flask_cors import CORS

from .controllers.index_controller import module
from .controllers.fraud_detection_controller import fraud_blueprint


webapp = Flask(__name__)
CORS(webapp)
webapp.config.from_object('config.config')

# webapp.register_blueprint(module)
webapp.register_blueprint(fraud_blueprint)

