import json

from flask import Blueprint, request, Response, jsonify
from flask_cors import cross_origin
from app.modules.model_processor import get_predictions


fraud_blueprint = Blueprint('fraud',
                            __name__,
                            url_prefix='/fraud',
                            )


@fraud_blueprint.route('/', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
# @auth.login_required
def index():
    return jsonify({"hello": "world"})


@fraud_blueprint.route('/predict', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
# @auth.login_required
def report():
    request_obj = request.get_json(force=True)

    response_obj = get_predictions(request_obj)

    if not response_obj['status'] == 200:
        response_payload = json.dumps({"message": response_obj["isFraud"]})
        return Response(response=response_payload,
                        status=response_obj['status'],
                        content_type='application/json')

    response_payload = json.dumps(response_obj)
    return Response(response=response_payload,
                    status=200,
                    content_type='application/json')
