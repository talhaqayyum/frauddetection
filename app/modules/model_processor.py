from config import config
from pycaret.classification import *
from ..modules import query_helper

dt_saved = load_model(config.xg_boost_model_all_types)


def get_response(message):
    return {
        "isFraud": bool(message),
        "status": 200
    }


def get_predictions(query):
    updated_query = query_helper.parsing_request(query)
    predictions = predict_model(dt_saved, data=updated_query)
    isFraudFlag = predictions['Label'].item()
    print(isFraudFlag)
    if isFraudFlag:
        return get_response(message=isFraudFlag)
    else:
        return get_response(message=isFraudFlag)
