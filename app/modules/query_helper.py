import pandas as pd


def get_request_type(query):
    if query['type'] == 'PAYMENT':
        query['type'] = 3
    elif query['type'] == 'CASH_OUT':
        query['type'] = 1
    elif query['type'] == 'CASH_IN':
        query['type'] = 0
    elif query['type'] == 'TRANSFER':
        query['type'] = 4
    elif query['type'] == 'DEBIT':
        query['type'] = 2
    return query


def parsing_request(query):
    request = get_request_type(query)
    request["errorbalanceOrig"] = request["newbalanceOrig"] + request["amount"] - request["oldbalanceOrig"]
    request["errorbalanceDest"] = request["oldbalanceDest"] + request["amount"] - request["newbalanceDest"]
    request.pop('nameOrig')
    request.pop('nameDest')
    #     print(query)
    df = pd.DataFrame([request])
    return df


