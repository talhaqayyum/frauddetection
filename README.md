# fraud_detection Api Documentation

# Index (/)
Default page for the fraud detection service.

# Hostname: 
Testing Url: http://34.239.246.66:4030/

# Endpoint 
fraud/

# Method: 
POST

# Authentication: 
None

# Request Params:
None

# Reponse:
Code: 200
Content: {"hello": "world"}
 
-------------------------------------------------------------------
 
 # fraud_prediction (/)
The purpose of this endpoint is to provide a prediction for fraud detection service.

# Hostname: 
Live: http://34.239.246.66:4030/

# Endpoint 
fraud/predict

# Method: 
POST

# Authentication: 
None

# Request Params:
{
    "step" : 1,
    "type" : "PAYMENT",
    "amount" : 9839,
    "nameOrig" : "CA21221",
    "oldbalanceOrig": 121212,
    "newbalanceOrig" : 160362,
    "nameDest" : "ADA12121",
    "oldbalanceDest" : 0,
    "newbalanceDest" : 0
}


# Reponse:
{
	"isFraud": false,
	"status": 200
}

