from app import webapp
from config import config

if __name__ == '__main__':
    webapp.run(host=config.app_host, port=config.app_port, debug=False)
